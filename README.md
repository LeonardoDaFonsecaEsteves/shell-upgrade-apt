**# Shell upgrade apt**

Script shell for update, upgrade & clean système Linux apt

## Create your aliase in root folder to use this script shell
    1. Edit **~/.bash_aliases** or **~/.bashrc** file using: ` vi ~/.bash_aliases`
    2. Append your bash alias
    3. For example append: alias MAJ='sudo ./path/for/script.sh'
    4. Save and close the file.
    5. Activate alias by typing: source ~/.bash_aliases

# Please note that ~/.bash_aliases file only works if the following line presents in the ~/.bashrc file:
    `if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
    fi`

## How to use/call aliases

 Just type alias name: `MAJ`